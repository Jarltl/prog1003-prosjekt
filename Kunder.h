#if !defined (__KUNDER_H)
#define __KUNDER_H

#include "ListTool2B.h"
#include "Kunde.h"

using namespace std;

class Kunder {
private:
	int sisteKunder;                     // Siste kundenummer brukt hittil

	List* KundeListe;                   // Liste av kunder

public: 
	Kunder();		                    
    void kundeHandling();
	void displayKundeHandling();
    void displayKundeAlle();
    void nyKunde();
	void endreKunde();
	char* hentNavn(int nr,char *nvn );
    bool finnKundeKjop(int knr);
    void lesFraFil(ifstream& inn);
	void skrivTilFil(ofstream& ut);

};



#endif
