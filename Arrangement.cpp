#define _CRT_SECURE_NO_WARNINGS

#define _CRT_SECURE_NO_WARNINGS
#include "Arrangement.h"
#include "const.h"
#include "ListTool2B.h"
#include "Sone.h"
#include "Stoler.h"
#include "Vrimle.h"
#include <iostream>
#include <fstream>
#include "globalFunksjonerHeadinger.h"
#include "Enums.h"
using namespace std;

Arrangement::Arrangement() { // Constructor
	cout << "Denne constructoren skal ikke brukes";
}

Arrangement::Arrangement(char* navn,int type, ifstream& inn) : TextElement(navn)
{
    //buffere
    char tempSpillested[NVNLEN];
    char tempArtist[STRLEN];
	int dag, maaned, aar;
    
    inn >> ArrNr; inn.ignore();
    
    inn.getline(tempSpillested, NVNLEN);//leser linje til buffer
    spilleSted = new char[strlen(tempSpillested) + 1];//setter av plass til navn
    strcpy(spilleSted, tempSpillested);//kopierer til
    
    inn.getline(tempArtist, NVNLEN);
    artist = new char[strlen(tempArtist) + 1];
    strcpy(artist,tempArtist);
    
	inn >> dag; inn.ignore(); 
	inn >> maaned; inn.ignore(); 
	inn >> aar;
	dato = (dag * 1000000) + (maaned * 10000) + aar; // Setter dd, mm og aa til en int
	

	inn >> Time; inn.ignore();
    inn >> minutt;
	inn >> oppsettNr;
    
    switch (type) {									 // Setter typen paa arrangementet 	
        case 0: arrType = Musikk;  break;
        case 1: arrType = Sport;   break;
        case 2: arrType = Teater;  break;
        case 3: arrType = Show;    break;
        case 4: arrType = Kino;    break;
        case 5: arrType = Familie; break;
        case 6: arrType = Festival;break;
    }
}

Arrangement::Arrangement(char* nvn, int nr, int oppNr) : TextElement(nvn) // Constructor
{
    char tempArtist[STRLEN];
    int type;
	int dager, maaned, aar;
    
    cout << "Arrangementnavn:" << text << endl;							  

	ArrNr = nr;
	oppsettNr = oppNr;
    
    les("Artist ", tempArtist, STRLEN);        // Leser til buffer
    artist = new char[strlen(tempArtist) + 1]; // Setter av plass til artist
    strcpy(artist, tempArtist);                // Kopierer til artist

	do
	{
		dager = les("Dager ", MINDAGER, MAXDAGER);
		maaned = les("Maaned", MINMAANED, MAXMAANED);
		aar = les("Aar", MINAAR, MAXAAR);
	} while (dagnummer(dager, maaned, aar) == 0); // Sjekker etter skuddaar
	dato = dager * 1000000 + maaned * 10000 + aar; // Setter dd, mm og aa til en int

    Time = les("Time ", MINTIME, MAXTIME);
  
    minutt = les("Minutt ", MINMINUTT, MAXMINUTT);
    
    cout << "\t\t0 = musikk" << endl
    << "\t\t1 = Sport" << endl
    << "\t\t2 = Teater" << endl
    << "\t\t3 = Show" << endl
    << "\t\t4 = Kino" << endl
    << "\t\t5 = Familie" << endl
    << "\t\t6 = Festival" << endl;
    
    type = les("\n\tSkriv inn arrangementtype ", 0, 6);
    cout << "\t\nNy hovedkommando: " << endl;
    
    switch (type) {                       // Setter typen paa arrangementet 	
        case 0: arrType = Musikk; break;
        case 1: arrType = Sport; break;
        case 2: arrType = Teater; break;
        case 3: arrType = Show; break;
        case 4: arrType = Kino; break;
        case 5: arrType = Familie; break;
        case 6: arrType = Festival; break;
    }

}

void Arrangement::display() // Skriver info om et arrangement ut p� skjermen
{
	int dag, maaned, aar;
	dag = dato / 1000000;
	maaned = (dato % 1000000) / 10000;
	aar = dato % 10000;

    cout << "Arrangementnavn: " << text << endl;     
    cout << "Arrangementnummer: " << ArrNr << endl;
    cout << "Spillested: " << spilleSted << endl;
    cout << "Artist: " << artist << endl;

	cout << "Dato: ";
	if (dag < 10)
	{
		cout << '0';
	}
	cout << dag << '/';
	if (maaned < 10)
	{
		cout << '0';
	}
	cout << maaned << '-' << aar << endl;
	cout << "Tid:  ";
	if (Time < 10)
	{
		cout << '0';
	}
	cout << Time << ':';
	if (minutt < 10)
	{
		cout << '0';
	}
	cout << minutt << endl << endl;

	cout << "Oppsettnummer: " << oppsettNr << endl;
    
    switch (arrType) {
        case 0: cout <<"Arrangementtype: Musikk"<< endl << endl; break;
        case 1: cout <<"Arrangementtype: Sport"<< endl << endl; break;
        case 2: cout <<"Arrangementtype: Teater"<< endl << endl; break;
        case 3: cout <<"Arrangementtype: Show"<< endl << endl; break;
        case 4: cout <<"Arrangementtype: Kino"<< endl << endl; break;
        case 5: cout <<"Arrangementtype: Familie"<< endl << endl; break;
        case 6: cout <<"Arrangementtype: Festival"<< endl << endl; break;
    }
}

bool Arrangement::finnesSpillested(char* nvn) // Finnes spillested
{
    return (!strcmp(spilleSted, nvn));        // Return true hvis char er like
}

bool Arrangement::finnesArtist(char* nvn)     // Finnes artist
{
    return (!strcmp(artist, nvn));            // Return true hvis char er like
}

bool Arrangement::finnesDato(int d)           // Finnes dato
{
    if (d == dato)
        return true;
    else
        return false;
}

bool Arrangement::finnesNr(int d)
{
	if (d == ArrNr)
		return true;
    else
        return false;
}

bool Arrangement::finnesArrNavn(char* nvn)    // Finnes arrangement navn    
{
    return (strstr(text, nvn));              // Return true hvis char er like
}

bool Arrangement::finnesArrtype(int type)		//Returnerer Arr type
{
    if (type == arrType)
        return true;
    else
            return false;
}

int Arrangement::hentNr()					//returnerer det unike ArrNummeret
{
	return ArrNr;
}

void Arrangement::lesData(List* liste, char* stedsnavn)	 //laster opp data til arrangement 
{
	spilleSted = new char[strlen(stedsnavn) + 1];		//setter av plass til navn
	strcpy(spilleSted, stedsnavn);						//kopierer til

	oppsettKopi = liste;
}

void Arrangement::slettOppsett()
{
	Sone* sone;
	Vrimle* vrimle;
	Stoler* stoler;
	int ant = oppsettKopi->noOfElements();
	 
	for (int i = 1; i <= ant; i++)					//For-loop gjennom alle soner i et oppsett
	{
		sone = (Sone*)oppsettKopi->removeNo(i);			//trekker ut og fjerner fra lista
		vrimle = (Vrimle*)oppsettKopi->removeNo(i);
		stoler = (Stoler*)oppsettKopi->removeNo(i);
	}
}

void Arrangement::registrereKjop(int knr, char* snavn)	//registrerer kjop for ett arrangement
{
	Sone* sone;
	oppsettKopi = hentListe();                          //henter liste fra ARR_.DTA


	sone = (Sone*)oppsettKopi->remove(snavn);           //peker po og fjerner sone
	if (sone->sjekkType() == 0) {                       //hvis sonetype er stoler
		oppsettKopi->add(sone);                         //legger tilbake
		Stoler* stoler;
		stoler = (Stoler*)oppsettKopi->remove(snavn);   //tar ut sone
		if (stoler->sjekkSolgt())                       //sjekker antall solgt
		{
			ofstream ut;
			ut.open("BILLETTER.DTA", ios::app);         //opner og printer ut info til BILLETTER.DTA
			ut << text << endl;
			ut << "Stoler" << endl;
			skrivTilFilBillett(ut);
			stoler->regBillett(knr);                    //registrerer billett
			ut.close();                                 //lukker fil

			oppsettKopi->add(stoler);                   //legger stolobjekt tilbake til lista
			oppsettKopi->displayElement(snavn);         //displayer sone
		}
		else
		{
			cout << endl << "ALLE BILLETTER UTSOLGT !!!" << endl << endl;
			oppsettKopi->add(stoler);
		}
	}
	else if (sone->sjekkType() == 1) {                  //samme som ovenpo men med vrimle
		oppsettKopi->add(sone);
		Vrimle* vrimle;
		vrimle = (Vrimle*)oppsettKopi->remove(snavn);
		if (vrimle->sjekkSolgt())
		{
			ofstream ut;
			ut.open("BILLETTER.DTA", ios::app);
			ut << text << endl;
			ut << "Vrimle" << endl;
			skrivTilFilBillett(ut);
			vrimle->regBillett(knr);
			ut.close();

			oppsettKopi->add(vrimle);
			oppsettKopi->displayElement(snavn);
		}
		else 
		{
			cout << endl << "ALLE BILLETTER UTSOLGT !!!" << endl << endl;
			oppsettKopi->add(vrimle);
		}
	}



}


List* Arrangement::hentListe()                          //retunerer liste fra ARR_.DTA fil
{
	char filNavn[STRLEN];
	char soneNavn[STRLEN];
	char buffer[STRLEN];
	char key[] = "Stoler";
	int antallSoner;
	int type;
	
	genererNavn(filNavn, "ARR_", ".DTA", ArrNr);       //genererer filnavn utifra ArrNr

	ifstream inn(filNavn);                              //hvis fil finnes
	if (inn.good()) {

		oppsettKopi = new List(Sorted);                 //ny liste sortert

		inn >> antallSoner;
		for (int j = 1; j <= antallSoner; j++)          //looper gjennom antall soner
		{
			inn >> buffer; inn.ignore();
			if (strcmp(key, buffer) == 0)               //hvis stoler
			{
				type = 0;
				inn >> soneNavn;
				oppsettKopi->add(new Stoler(soneNavn, type, inn));  //lager nytt stolobjekt
			}

			else if (strcmp(key, buffer) != 0)                  //hvis vrimle
			{
				type = 1;
				inn >> soneNavn;
				oppsettKopi->add(new Vrimle(soneNavn, type, inn));  //lager nytt vrimleobjekt

			}
		}
		
	}
	return oppsettKopi;
}

void Arrangement::skrivTilFil(ofstream& ut)         //skrivet ett arrangement til fil
{
	int dag, maaned, aar;
	dag = dato / 1000000;                           //gjor om dag, mnd, ar til DDMMAAAA
	maaned = (dato % 1000000) / 10000;
	aar = dato % 10000;


    ut << text << endl;
    switch (arrType) {                          //skriver ut arrtype
        case 0: ut << "Musikk" << endl; break;
        case 1: ut << "Teater" << endl; break;
        case 2: ut << "Sport" << endl; break;
        case 3: ut << "Show" << endl; break;
        case 4: ut << "Kino" << endl; break;
        case 5: ut << "Familie" << endl; break;
        case 6: ut << "Festival" << endl; break;
    }
    ut << ArrNr << endl;
    ut << spilleSted << endl;
    ut << artist << endl;
	if (dag < 10)
	{
		ut << '0';
	}
	ut << dag << '/';
	if (maaned < 10)
	{
		ut << '0';
	}
	ut << maaned
	   << '-' << aar << endl;
	if (Time < 10)
	{
		ut << '0';
	}
	ut << Time << ':';
	if (minutt < 10)
	{
		ut << '0';
	}
	ut << minutt << endl;

	ut << oppsettNr << endl << endl;
}

void Arrangement::skrivTilFilOppsett(ofstream & ut)
{
	Sone* sone;

		ut << oppsettKopi->noOfElements() << endl << endl;
		for (int i = 1; i <= oppsettKopi->noOfElements(); i++) //looper gjennom antall oppsett
		{
			sone = (Sone*)oppsettKopi->removeNo(i);             //peker og fjerner
			if (sone->sjekkType() == 0)                         //hvis sonetype 0 stoler
			{
				oppsettKopi->add(sone);

				Stoler* stolerOB;
				stolerOB = (Stoler*)oppsettKopi->removeNo(i);    //peker po stolobjekt og fjerner fra liste
				stolerOB->skrivTilFilOppsett(ut);               //skriver til fil
                oppsettKopi->add(stolerOB);                     //legger tilbake
			}
			else if (sone->sjekkType() == 1)                      //hvis sonetype 1 vrimle
			{
				oppsettKopi->add(sone);
				Vrimle* vrimleOB;
				vrimleOB = (Vrimle*)oppsettKopi->removeNo(i);           //peker po stolobjekt og fjerner fra liste
				vrimleOB->skrivTilFil(ut);                             //skriver til fil
				oppsettKopi->add(vrimleOB);                            //legger tilbake
			}

		}
}

void Arrangement::skrivTilFilBillett(ofstream & ut)     //skriver ut dato, tid, artist og spille sted til BILLETTER.DAT
{
	int dag, maaned, aar;
	dag = dato / 1000000;                               //gjor om dag, mnd, ar til DDMMAAAA
	maaned = (dato % 1000000) / 10000;
	aar = dato % 10000;

	ut << "Dato: "; 
	if (dag < 10)
	{
		ut << '0';
	}
	ut << dag << '/';
	if (maaned < 10)
	{
		ut << '0';
	}
	ut << maaned << '-'
	   << aar << endl;
	ut << "Tid: "; 
	if (Time < 10)
	{
		ut << '0';
	}
	ut << Time << ":"; 
	if (minutt < 10)
	{
		ut << '0';
	}
	ut << minutt << endl;
	ut << "Artist: " << artist << endl;
	ut << "Spillested: " << spilleSted << endl << endl;


}

