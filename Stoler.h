#if !defined (__STOLER_H)
#define __STOLER_H

#include "ListTool2B.h"
#include "Sone.h"
#include "globalFunksjonerHeadinger.h"


class Stoler: public Sone {
private:
	int antallRader, antallSeter;
	int bilett[MAXRADER][MAXSETER];
   
public:
	Stoler(char * navn, int type);
	Stoler(char * navn, int type, ifstream& inn);
	Stoler(Stoler& s, int type);
	void regBillett(int knr);
	void display();
	bool sjekkSolgt();
	void skrivTilFil(ofstream& ut);
	void skrivTilFilOppsett(ofstream & ut);
};



#endif


