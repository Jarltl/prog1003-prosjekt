#include <iostream>
#include <fstream>
#include "Vrimle.h"
#include "const.h"
#include "globalFunksjonerHeadinger.h"
using namespace std;


Vrimle::Vrimle(char * navn, int type) : Sone(navn, type) 
{

}

Vrimle::Vrimle(char * navn, int type, ifstream & inn) : Sone(navn, type, inn)
{
	
}

Vrimle::Vrimle(Vrimle & v, int type) : Sone((Sone*) & v, type)
{
 
}

void Vrimle::display() {

	Sone::display();
	cout << endl;
}

bool Vrimle::sjekkSolgt()                           //hvis tilgjengelig billetter return true
{
	if (solgt < totaltilsalgs)
		return true;
	else
		return false;
}

void Vrimle::regBillett(int knr)                    //registrerer billettkjop på vrimle
{
	int ant, tilg;
	tilg = totaltilsalgs - solgt;                   //arver solgt fra sone og setter tilg til billetter igjen

	if (solgt < totaltilsalgs) {                    //hvis solgt er mindre enn total til salgt
		ant = les("Antall billetter: ", 1, tilg);   //leser inn antall billetter som onskes
		for (int i = 1; i <= ant; i++) {

			solgt++;                                //oker solgte
			billett[solgt] = knr;                   //knytter kundenummer med solgte billetter

		}

		cout << endl << endl << "Du har kjopt " << ant << " billetter." << endl <<endl;
		ofstream ut;
		ut.open("BILLETTER.DTA", ios::app);                  //opner DTA fil og skriver på slutten av filen
		ut << "Antall billetter: " << ant << endl << endl;
	}
	else
	{
		cout << endl << "ALLE BILLETTER UTSOLGT !!!" << endl << endl;
	}
}

void Vrimle::skrivTilFil(ofstream & ut)
{
	Sone::skrivTilFil(ut);
	ut << endl;
}
