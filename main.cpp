
#include "globalFunksjonerHeadinger.h"
#include "Kunder.h"
#include "Arrangementer.h"
#include "Steder.h"
#include "Sted.h"

#include <iostream>

using namespace std;

Kunder kunder;
Steder steder;
Arrangementer arrangementer;

int main() {

	char ch;							//char for valg

	lesFraFil();                        //leser fra fil ved oppstart
	skrivMeny();
	ch = lesKommando();					//les kommando

	while (ch != 'Q')					//s� lenge valg ikke lik Q
	{
		switch (ch)						//switch med valg som parameter   
		{

            case 'K': kunder.kundeHandling(); break;	    //Kaller
            case 'S': steder.stedHandling(); break;	        //Kaller
			case 'O': steder.oppsettHandling(); break;	    //Kaller
            case 'A': arrangementer.arrHandling(); break;	//Kaller
		default:  skrivMeny(); break;	                    //skriver meny
		}
		
		ch = lesKommando();				//les kommando
	}
	skrivTilFil();                      //skriver til fil(er) om 'q' tastes
	return 0;
}
