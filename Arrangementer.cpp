#include "Arrangementer.h"
#include "Arrangement.h"
#include "globalFunksjonerHeadinger.h"
#include "const.h"
#include "Enums.h"
#include "ListTool2B.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include "Steder.h"
#include "Sted.h"
#include "Kunder.h"
#include <cstdlib>
#include <time.h>
#include <stdlib.h>
#include <conio.h>

extern Steder steder;
extern Kunder kunder;
using namespace std;

Arrangementer::Arrangementer() { // Constructor

	ArrListe = new List(Sorted); // Lager en ny sortert liste ArrListe

}

void Arrangementer::arrHandling() // Inneholder de forskjellige arrangementer handlingene
{
	char ch;            // 
	ch = lesKommando();

	switch (ch)					
	{
	case 'D': displayArrHandling(); break;		
	case 'N': nyttArr(); break;
    case 'S': slettArr(); break;
    case 'K': kjopArr(); break;


	}
}

 void Arrangementer::displayArrHandling()
{
    char ch;                            //char for valg
    
    skrivMenyArr();
    ch = lesKommando();                    //les kommando
    
    while (ch != 'Q')                    //s� lenge valg ikke lik Q
    {
        switch (ch)                        //switch med valg som parameter
        {
            case '1': displayArrAlle();    break;
            case '2': displayArrNavn();    break;
            case '3': displaySpillested(); break;
            case '4': displayMedDato();    break;
            case '5': displayType();       break;
            case '6': displayMedArtist();  break;
            case '7': displayMedNr();      break;
        default:  skrivMenyArr();          break;    //skriver meny
        }
        cout << "\n\nKommando: ";
        ch = lesKommando();                //les kommando
    }
    skrivMeny();
}

void Arrangementer::skrivMenyArr()
{
    cout << "1                   - hoveddata om ALLE" << endl;
    cout << "2 / <arr.(del)navn> - hoveddata om de(t) akuelle arrangement" << endl;
    cout << "3 / <spillested>    - hoveddata om arrangement pa spillested" << endl;
    cout << "4 / <dato>          - hoveddata om arrangement pa dato" << endl;
    cout << "5 / <type arr.>     - hoveddata om arrangement av viss type" << endl;
    cout << "6 / <artistnavn>    - hoveddata om arrangement med artist" << endl;
    cout << "7 / <nr>            - ALLE data om ETT" << endl;
    
    cout << "Q - Quit for a ga til hovedmenyen\n\n";
    cout << "\n\nKommando: ";


}

void Arrangementer::displayArrAlle()
{
	ArrListe->displayList();		
}

void Arrangementer::displayArrNavn()
{
    char tekst[NVNLEN];
    Arrangement* arrangementpeker;   //arrangement peker
    bool fant = false;              //brukes for sjekk av arrangement
    
    les("Navn ", tekst, NVNLEN);
    
    for (int index = 1; index <= ArrListe->noOfElements(); index++) //loop antall element
    {
        arrangementpeker = (Arrangement*)ArrListe->removeNo(index);      //ta ut nr i i lista
        
        if (arrangementpeker->finnesArrNavn(tekst)) {               //hvis arrangementnavn finnes
            cout << "HOVEDDATA OM DET/DE AKTUELLE ARRANGEMENTET(ENE) MED: " << tekst << "\n";
            arrangementpeker->display();                        //displayer arr med navn
            ArrListe->add(arrangementpeker);					//legger tilbake i lista
            fant = true;                                        //fant arr
        }
        else {
            ArrListe->add(arrangementpeker);					//fant ikke navn, legger tilbake
        }
    }
    if (!fant) {												//fant ikke navn, legger tilbake
        cout << "\nFinnes ikke!";
    }
}

void Arrangementer::displaySpillested()
{
    char tekst[NVNLEN];
    Arrangement* arrangementpeker;
    bool fant = false;
    
    les("Spillested ", tekst, NVNLEN);
    
    for (int index = 1; index <= ArrListe->noOfElements(); index++)     //loop antall element
    {
        arrangementpeker = (Arrangement*)ArrListe->removeNo(index);      //ta ut nr i i lista
        
        if (arrangementpeker->finnesSpillested(tekst)) {                 //hvis <spillested> finnes
            cout << "HOVEDDATA OM SPILLESTED: " << tekst << "\n";
            arrangementpeker->display();
            ArrListe->add(arrangementpeker);
            fant = true;
        }
        else {
            ArrListe->add(arrangementpeker);
        }
    }
    if (!fant) {
        cout << "\nFinnes ikke!";
     }
}

void Arrangementer::displayMedNr()
{
	int n;
	Arrangement* peker;
	List* liste;
	bool fant = false;

	cout << "Skriv unikt arrangementnummer: ";
	cin >> n;

	for (int index = 1; index <= ArrListe->noOfElements(); index++) {    //loop antall element
		peker = (Arrangement*)ArrListe->removeNo(index);                 //ta ut nr i i lista

		if (peker->finnesNr(n)) {                                        //hvis nummer finnes
			cout << "ALLE DATA OM ARRANGEMENTNR: " << n << "\n";
			peker->display();
			liste = peker->hentListe();
			liste->displayList();
			ArrListe->add(peker);
			fant = true;
		}
		else {
			ArrListe->add(peker);
		}
	}
	if (!fant) {
		cout << "\nFinnes ikke!";
	}
}

void Arrangementer::displayMedDato()
{
    Arrangement* peker;
    bool fant = false;
	int dager, maaned, aar;
	int dato;

    
	do
	{
		dager = les("Dager ", MINDAGER, MAXDAGER);
		maaned = les("Maaned", MINMAANED, MAXMAANED);
		aar = les("Aar", MINAAR, MAXAAR);
	} while (dagnummer(dager, maaned, aar) == 0);	// Sjekker etter skuddaar
	dato = dager * 1000000 + maaned * 10000 + aar;	// Setter dd, mm og aa til en int
    
    for (int index = 1; index <= ArrListe->noOfElements(); index++) { //loop antall element
        peker = (Arrangement*)ArrListe->removeNo(index);      //ta ut nr i i lista
        
        if (peker->finnesDato(dato)) {                  //om det er <dato>
            cout <<endl<< "DATA OM ARRANGEMENT PA DATO: " << dato << "\n";
            peker->display();
            ArrListe->add(peker);
            fant = true;
        }
        else {
            ArrListe->add(peker);
        }
    }
    if (!fant) {
        cout << "\nFinnes ikke!";
    }
}


void Arrangementer::displayType()
{
    Arrangement* peker;
    bool fant = false;
    
    cout << "Velg type:\n\n";
    cout << "Musikk   = 0\n";
    cout << "Sport    = 1\n";
    cout << "Teater   = 2\n";
    cout << "Show     = 3\n";
    cout << "Kino     = 4\n";
    cout << "Familie  = 5\n";
    cout << "Festival = 6\n";

    int nr = les("Kommando", 0, 6);
    
        for (int index = 1; index <= ArrListe->noOfElements(); index++) { //loop antall element
            
            peker = (Arrangement*)ArrListe->removeNo(index);      //ta ut nr i i lista
            
            if (peker->finnesArrtype(nr)) {                  //om det er skrevet inn 0-6
                cout << "DATA OM EN VISS TYPE\n\n ";
                peker->display();
                ArrListe->add(peker);
                fant = true;
            }
            else {
                ArrListe->add(peker);
                }
        }
    if (!fant) {
        cout << "\nFinnes ikke!";
    }
}

void Arrangementer::displayMedArtist()
{
    char tekst[NVNLEN];
    Arrangement* arrangementpeker;
    int teller = 0;
    
    les("Artist ", tekst, NVNLEN);
    
    for (int index = 1; index <= ArrListe->noOfElements(); index++) //loop antall element
    {
        arrangementpeker = (Arrangement*)ArrListe->removeNo(index);      //ta ut nr i i lista
        
        if (arrangementpeker->finnesArtist(tekst)) {                //hvis <artistnavn> finnes
            cout << "HOVEDDATA OM ARRANGEMENT MED ARTIST: " << tekst << "\n";
            arrangementpeker->display();
            ArrListe->add(arrangementpeker);
            teller++;
        }
        else {
            ArrListe->add(arrangementpeker);
        }
    }
    if (teller == 0) {
        cout << "\nFinnes ikke!";
        }
}


void Arrangementer::displayArrTekst(char* nvn)
{
	ArrListe->displayElement(nvn);
}

void Arrangementer::nyttArr()
{
	char anavn[STRLEN], snavn[STRLEN], arrfil[STRLEN];
	int oppsettNr, ant, arrNr;
	Arrangement* arrangement;
	List* liste;

	les("Sted", snavn, STRLEN);									//leser stedsnavn
	
	if (!steder.finnesSted(snavn))								//sjekker om sted med gitt navn finnes
	{
		cout << "Ingen sted med dette navnet" << endl;
	}	
	else if (!steder.finnesOppsett(snavn))						//sjekker om sisteOppsett > 0
		cout << "Ingen oppsett registrert pa dette stedet" << endl;
	else {
		ant = steder.hentAntOppsett(snavn);						//henter antall oppsett fra steder

		les("Nytt arrangements navn ", anavn, STRLEN);	
		oppsettNr = les("Oppsettsnummer ", 1, ant);				//leser int antall oppsett

		arrNr = genererNr();									//generer et tilfeldig og unikt nummer
		for (int i = 1; i <= ArrListe->noOfElements(); i++)		
		{
			arrangement =(Arrangement*) ArrListe->removeNo(i);	//tar ut arrangement fra lista
			if (arrangement->finnesNr(arrNr)) {					//sjekker om det nye genererte nummeret finnes allerede i lista
				arrNr = genererNr();							//hvis det finnes, generer et nytt nummer
			}
			ArrListe->add(arrangement);							//legger tilbake i lista
		}

		arrangement = new Arrangement(anavn, arrNr, oppsettNr); //lager et nytt arrangement

		sisteArrNr++;											//teller opp siste arrangementnummer 

		liste = steder.kopier(snavn, oppsettNr);				//kopierer oppsettet
		arrangement->lesData(liste, snavn);				//laster opp data til arrangement 

		arrNr = arrangement->hentNr();					//henter arrNr;
		genererNavn(arrfil, "ARR_", ".DTA", arrNr);		//generer et nytt filnavn

		ofstream ut(arrfil);							//Lager ey nytt fil

		arrangement->skrivTilFilOppsett(ut);			//skriver ut til ARR_NR fil 
		arrangement->slettOppsett();					//sletter oppsettet fra mimme 

		ArrListe->add(arrangement);						//legger i lista
	}
}

int Arrangementer::genererNr()
{
	int nr;

	srand(time(NULL));				//	Knytter verdien til kl.slett til rand funkasjonen
	nr = rand() % 100 + 1;

	return nr;
}


void Arrangementer::slettArr()
{
    char anvn[NVNLEN];
    char svar;
    Arrangement* arrangementpeker;
    char arrfil[STRLEN];
    int arrNr;
    
    cout << "Slett et arrangement: ";
    cin.getline(anvn, NVNLEN);
    if (ArrListe->inList(anvn)) {                               //hvis arrangement er i liste
        
        cout << "\nSikker? (J/N)\n";                            //sikring for sletting av arrangement J/N
        svar = lesKommando();
        
        switch (svar) {
            case 'J':
                arrangementpeker = (Arrangement*)ArrListe->remove(anvn);	//Tar ut arrangement med navnet 'anvn'
                cout << "\nDu har fjernet arrangement!";
                
                arrNr = arrangementpeker->hentNr();							//Henter arr nr
                genererNavn(arrfil, "ARR_", ".DTA", arrNr);
                
                if (!remove(arrfil)){									//hvis fila finnes, sletter den
                    cout << "\nOppsett til " << arrfil << " har blitt slettet!\n\n\n" ;
                }
                else
                    cout << "\nIngen oppsettfil som slettes pa arrangemanget";
                break;
            
            case 'N': cout << "\nSletter ikke!"; break;
            
            default: cout << "\nFeil kommando! Sikker? (J/N)\n"; break;
        }
    }
    else cout << "\nFinner ikke arrangement\n";
         cout << "\n\nSkriv ny hovedkommando: ";
}


void Arrangementer::kjopArr()
{
	char anvn[STRLEN];
	char soneNavn[STRLEN];
	char arrFil[STRLEN];
	char  kundenavn[STRLEN];
	int knr, arrNr;
	Arrangement* arrangementpeker;
	List* listepeker;

	do {										
		cout << "\nKjop pa arrangement: ";
		cin.getline(anvn, NVNLEN);
	} while (!ArrListe->inList(anvn));                          //looper til arr finnes

	arrangementpeker = (Arrangement*)ArrListe->remove(anvn);	//Tar ut fra liste
	arrangementpeker->display();								//Displayer
	ArrListe->add(arrangementpeker);							//Legger tilbake

	listepeker = arrangementpeker->hentListe();					//Henter og viser liste
	listepeker->displayList();

	do {
		cout << "\nPa onsket sonenavn: ";
		cin.getline(soneNavn, STRLEN);
	} while (!listepeker->inList(soneNavn));					//Looper til unikt navn

	do {
		cout << "\nKundenummer: ";
		cin >> knr;
	} while (!kunder.finnKundeKjop(knr));						//looper gyldig kundenr

	arrangementpeker->registrereKjop(knr, soneNavn);		

	ofstream utbill;
	utbill.open("BILLETTER.DTA", ios::app);
	utbill << "Kunde: " << kunder.hentNavn(knr, kundenavn) << endl;
	utbill << "Kundenummer: " << knr << endl << endl << endl << endl;
	utbill.close();

	arrNr = arrangementpeker->hentNr();							//Henter arrangementetes nr

	genererNavn(arrFil, "ARR_", ".DTA", arrNr);					//Genrerer filnavn pa utfil
	ofstream ut(arrFil);

	arrangementpeker->skrivTilFilOppsett(ut);					//Arrangement skriver seg selv ut


	cout << "Trykk ENTER for a ga tilbake til HOVEDMENYEN" << endl;

	_getch();											//Venter paa input fra bruker
	_getch();
	skrivMeny();
}


void Arrangementer::lesFraFil(ifstream& inn)
{
    int antallArr = 0;                  //antall arr
    char arrNavn[STRLEN];
    char typeTekst[STRLEN];
    int type;
    inn >> antallArr; inn.ignore();     //leser antall arr i filen
        
    if (antallArr > 0) {
        
        for (int i = 1; i <= antallArr; i++) { //loop gjennom alle arrangementer
            inn >> arrNavn;                     //leser arrnavn
            inn >> typeTekst;
            
            if (strcmp(typeTekst, "Musikk") == 0)       //hvis typetekst er Musikk blir type '0'
                type = 0;
            else if (strcmp(typeTekst, "Sport") == 0)
                type = 1;
            else if (strcmp(typeTekst, "Teater") == 0)
                type = 2;
            else if (strcmp(typeTekst, "Show") == 0)
                type = 3;
            else if (strcmp(typeTekst, "Kino") == 0)
                type = 4;
            else if (strcmp(typeTekst, "Familie") == 0)
                type = 5;
            else if (strcmp(typeTekst, "Festival") == 0)
                type = 6;
            
            ArrListe->add(new Arrangement(arrNavn, type, inn)); //adder nytt arrangement til liste
        }
    }
        cout << " Lest inn: " << antallArr << " fra ARRANGEMENTER.DTA" << endl;
}


void Arrangementer::skrivTilFil(ofstream& ut)
{
    int ant = 0;
    ant = ArrListe->noOfElements();						    //henter antall objekter i lista
    
    if (ant > 0) {
        ut << ant << endl << endl;						    //skriver ut antall
        Arrangement* peker;								    //lager en ny peker til
        for (int i = 1; i <= ant; i++) {				    //looper gjennom alle
            peker = (Arrangement*)ArrListe->removeNo(i);    //"trekker ut"
            peker->skrivTilFil(ut);                         //skriver den ut til fil
            ArrListe->add(peker);                           //legger den tilbake
            
        }
    }
}
