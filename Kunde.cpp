#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif 

#include "Kunde.h"
#include "const.h"
#include <iostream>
#include <fstream>
#include "globalFunksjonerHeadinger.h"
#include <cstring>

using namespace std;

                                                //skriver ut informasjon om kunde
void Kunde::display(){
	cout << "Kundenummer: " << number << endl;
	cout << "Kundenavn: " << kundenavn << endl;
	cout << "Adresse: " << gateadresse << endl;
	cout << "Poststed: " << poststed << endl;
	cout << "Postnummer:" << postnummer << endl;
	cout << "Tlf:" << tlf << endl;
    cout << "Email: " << email << endl << endl;
}

Kunde::Kunde(int nr) : NumElement(nr){   //constructer ny kunde med nr fra listTool
    
	char tempNavn[NVNLEN];                //buffere
	char tempAdresse[STRLEN];
	char tempSted[STRLEN];
	char tempMail[STRLEN];

	
	cout << "Kundenummer:" << number << endl;   //skriver ut kundenummer

	les("Kundenavn ", tempNavn, NVNLEN);	    //leser til buffer
	kundenavn = new char[strlen(tempNavn) + 1]; //setter av plass til navn
	strcpy(kundenavn, tempNavn);                //kopierer til kundenavn

	les("Adresse ", tempAdresse, STRLEN);
	gateadresse = new char[strlen(tempAdresse) + 1];
	strcpy(gateadresse, tempAdresse);

	les("Poststed ", tempSted, STRLEN);
	poststed = new char[strlen(tempSted) + 1];
	strcpy(poststed, tempSted);

	postnummer = les("Postnummer ", MINPOST, MAXPOST);

	tlf = les("Telefonnummer ", MINTLF, MAXTLF);

	les("Email ", tempMail, STRLEN);
	email = new char[strlen(tempMail) + 1];
	strcpy(email, tempMail);

    }


Kunde::Kunde(int nr, ifstream& inn) : NumElement(nr) {   //constructor med innfil som parameter
	
	char tempNavn[NVNLEN];                  //buffere
	char tempAdresse[STRLEN];
	char tempSted[STRLEN];
	char tempMail[STRLEN];


	inn.getline(tempNavn, NVNLEN);               //leser linje til buffer
	kundenavn = new char[strlen(tempNavn) + 1];  //setter av plass til navn
	strcpy(kundenavn, tempNavn);                 //kopierer til kundenavn

	inn.getline(tempAdresse, NVNLEN);
	gateadresse = new char[strlen(tempAdresse) + 1];
	strcpy(gateadresse,tempAdresse);

	inn >> postnummer; inn.ignore();

	inn.getline(tempSted, NVNLEN);
	poststed = new char[strlen(tempSted) + 1];
	strcpy(poststed, tempSted);

	inn >> tlf; inn.ignore();

	inn.getline(tempMail, NVNLEN);
	email = new char[strlen(tempMail) + 1];
	strcpy(email, tempMail);

}


int Kunde::returnNr()                        //returner kunde number listTool
{
    return number;
}

bool Kunde::finnesNummer(int n)           // Finnes dato
{
    if (n == number)
        return true;
    else
        return false;
}

char*  Kunde::hentNavn()                //retunerer navn pa kunde
{
	return kundenavn;
}

bool Kunde::finnesNavn(char* nvn)            //finnes kundenavn
{
    return (!strcmp(kundenavn, nvn));        //return true hvis char er like
}


                                            //Funksjon som skriver info om kunde til fil
void Kunde::skrivTilFil(ofstream& ut) {
	ut << number << endl	                 //unikt kundenummer
		<< kundenavn << endl
		<< gateadresse << endl
		<< postnummer << endl
		<< poststed << endl
		<< tlf << endl
		<< email << endl << endl;
	

}
