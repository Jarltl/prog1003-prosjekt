#include <iostream>
#include <fstream>
#include "Sone.h"
#include "const.h"
#include "Sted.h"
#include "Vrimle.h"
#include "Stoler.h"
#include "globalFunksjonerHeadinger.h"
using namespace std;

Sone::Sone()											//constructor
{
	solgt;
	totaltilsalgs = les("Total antall billetter til salgs: ", MINBILLETT, MAXBILLETT);
	pris = les("Pris per billett: ", MINPRIS, MAXPRIS);
}

Sone::Sone(char * navn, int type) :TextElement(navn)	//constructor med navn og type som parameter
{
	solgt;
	totaltilsalgs = les("Total antall billetter til salgs: ", MINBILLETT, MAXBILLETT);
	pris = les("Pris per billett: ", MINPRIS, MAXPRIS);


                
	switch (type)										//setter type til stoler eller vrimler
	{
	case 0: soneType = stoler; break;
	case 1: soneType = vrimle; break;
	}

}

Sone::Sone(char * navn, int type, ifstream & inn) :TextElement(navn)
//constructor som leser inn fra fil

{
	inn >> totaltilsalgs;
	inn >> pris;
	inn >> solgt;

	switch (type)
	{
	case 0: soneType = stoler; break;
	case 1: soneType = vrimle; break;
	}
}

Sone::Sone(Sone * s, int type) : TextElement(s->text)
// constructor med Sone objekt som parameter

{
	totaltilsalgs = s->totaltilsalgs;
	solgt = s->solgt;
	pris = s->pris;

	switch (type)
	{
	case 0: soneType = stoler; break;
	case 1: soneType = vrimle; break;
	}
}

void Sone::display(){
	cout << "Sonetype: ";

	switch (soneType)
	{
	case 0: cout << "Stoler " << endl; break;
	case 1: cout << "Vrimle " << endl; break;
	}
		

	cout << "Sonenavn: " << text << endl;
	cout << "Billetter til salgs: " << totaltilsalgs << endl;
	cout << "Pris per billett: " << pris << endl;
	cout << "Solgt: " << solgt << endl;

}

void Sone::skrivTilFil(ofstream& ut)
{
	switch (soneType)
	{
	case 0: ut << "Stoler " << endl; break;
	case 1: ut << "Vrimle " << endl; break;
	}
	ut << text << endl;
	ut << totaltilsalgs << endl;
	ut << pris << endl;
	ut << solgt << endl;
	

}

bool Sone::sjekkType()				//returnere type
{
	return soneType;
}

bool Sone::hentNavn(char* nvn)		
{
	nvn = text;

	return nvn;					//returnerer sonenavn
}

