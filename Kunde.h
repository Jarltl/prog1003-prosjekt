#if !defined(__KUNDE_H)
#define  __KUNDE_H

#include "ListTool2B.h"
#include <fstream>

using namespace std;

class Kunde : public NumElement {
private:
	int postnummer,
		tlf;
	char* kundenavn,
		*gateadresse,
		*poststed,
		* email;
public:
	Kunde(int nr);	                     
	Kunde(int nr, ifstream& inn);        
    void display();
    int returnNr();
    bool finnesNummer(int n);
    char* hentNavn();
    bool finnesNavn(char* nvn);
	void skrivTilFil(ofstream& ut);     
	};

#endif
