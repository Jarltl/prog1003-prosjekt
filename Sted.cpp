#include <iostream>
#include <fstream>
#include <cstring>
#include "Sted.h"
#include "const.h"
#include "globalFunksjonerHeadinger.h"
#include "Vrimle.h"
#include "Stoler.h"
using namespace std;

Sted::Sted()
{
}

void Sted::display(){                               //displayer stedsnavn og antall oppsett per sted
    cout << "\n\n\nStedsnavn: " << text << endl;
	cout << "Antall oppsett: " << sisteOppsett << endl << endl;
}


Sted::Sted(char* navn) :TextElement(navn) {
	
}

Sted::Sted(char* navn, ifstream & inn) : TextElement(navn)
{
	char buffer[STRLEN];
	char key[] = "Stoler";
	char soneNAVN[NVNLEN];
	int type;
	int oppsettNr;
	int antallSoner;

	inn >> sisteOppsett;
	inn.ignore();
	inn.ignore();

	if (sisteOppsett > 0) 
	{	
		for (int i = 1; i <= sisteOppsett; i++)                              
		{


			inn >> oppsettNr;
			inn >> antallSoner;



			oppsett[oppsettNr] = new List(Sorted);                           // Lager ny liste

			for (int j = 1; j <= antallSoner; j++)                           // Leser Stoler eller Vrimle fra fil
			{
				inn >> buffer; inn.ignore();
				if (strcmp(key, buffer) == 0)                                // Sjekker om buffer er lik Stoler
				{
					type = 0;                                                // 0 = Stoler
					inn >> soneNAVN;
					oppsett[oppsettNr]->add(new Stoler(soneNAVN, type, inn)); // Legger til nytt Stoler objekt
				}

				else if (strcmp(key, buffer) != 0)                            // Sjekker om buffer ikke er lik Stoler
				{
					type = 1;                                                 // 1 = Vrimle
					inn >> soneNAVN;
					oppsett[oppsettNr]->add(new Vrimle(soneNAVN, type, inn)); // Legger til nytt Vrimle objekt

				}
			}
		}
	}
}

void Sted::nyttOppsett() 
{
	char ch;             

	if (sisteOppsett <= MAXOPPSETT)                                  // Lager ny oppsett
	{
		++sisteOppsett;           
		oppsett[sisteOppsett] = new List(Sorted);                    // Lager ny liste
		cout << "Oppsett laget" << endl;          

		cout << "Oppsett: T(otalt nytt) , K(opiert og endret paa):";
		ch = lesKommando(); 

		switch (ch)                                                  // Velger mellom � lage ny sone og kopiere oppsett
		{
		case'T':nySone(sisteOppsett); break; 
		case'K':kopierOppsett(sisteOppsett); break; 
		default: cout << "Feil N-kommando!\n";  break; 
		}
	}
    else
		cout << "Ikke plass til flere.";
    
}

void Sted::endreOppsett()
{
	int nr;
	char ch; 

	if (sisteOppsett > 0) {

		do {
			nr = les("Hvilket oppsett onsker du aa endre paa?", MINOPPSETT, sisteOppsett);// Parameter som blir sendt videre


		} while (nr > sisteOppsett);

		cout << "Oppsett nr: " << nr << endl;
		oppsett[nr]->displayList();                                  // Skriver ut data om et spesifikt oppsett

		cout << endl << "Oppsett: L(egge til soner), F(fjerne soner), E(endre soner),  Q(Ga til hovedmenyen)" << endl;
        cout << "\nDitt valg: ";
		ch = lesKommando();

		while (ch != 'Q')		                                                                                       
		{

			switch (ch)                                          // Endring av sone valg
			{
			case 'L':nySone(nr);	break;
			case 'F':fjernSone(nr); break;
			case 'E':endreSone(nr);	break;
			default:
            cout << endl << "Oppsett: L(egge til soner), F(fjerne soner), E(endre soner), Q(Ga til hovedmenyen)" << endl;
                    break;
			}
			cout << "\n\nKommando: ";
			ch = lesKommando();
		}

	}
	else
		cout << "Finnes ingen oppsett" << endl;
	skrivMeny();
}

void Sted::endreOppsett(int nr)     // Nesten lik endreOppsett(), men blir kalt med oppsett nummer som parameter
{
	char ch;

	if (sisteOppsett > 0) {

		cout << endl << "Oppsett: L(egge til soner), F(fjerne soner), E(endre soner),  Q(Ga til hovedmenyen)" << endl;

		ch = lesKommando();

		while (ch != 'Q')
		{

			switch (ch)
			{
			case 'L':nySone(nr);	break;
			case 'F':fjernSone(nr); break;
			case 'E':endreSone(nr);	break;
			default:
				cout << endl << "Oppsett: L(egge til soner), F(fjerne soner), E(endre soner), Q(Ga til hovedmenyen)" << endl;
				break;
			}
			cout << "\n\nKommando: ";
			ch = lesKommando();
		}

	}
	else
		cout << "Finnes ingen oppsett" << endl;
	skrivMeny();
}

int Sted::hentAntOppsett(int ant)
{
	ant = sisteOppsett;              // Antall oppsett = tallet til det siste oppsettet
	return ant;
}

void Sted::kopierOppsett(int OppNrr) // Oppsett nummer som parameter
{
	char valg;
	int nr;

	if (sisteOppsett > 0) {

		do {
			nr = les("Hvilket oppsett onsker du a kopiere?", MINOPPSETT, sisteOppsett-1);
			
		} while (nr > sisteOppsett);

		cout << endl << "Oppsett nr: " << nr << endl;
		oppsett[nr]->displayList();                                      // Displayer data om et spesifikt oppsett


		oppsett[OppNrr] = kopier(nr);                                    // Oppsettet som er sendt med blir satt lik det valgte oppsett

		cout << endl <<"Onsker du a endre pa? J(a) eller N(ei)" << endl;
		valg = lesKommando();

		if (valg == 'J')                                                 // Hvis Ja, blir du tatt til funksjonen endreOpsett()
		{
			endreOppsett(OppNrr);                                        
		}

	}
	else
		cout << "Finnes ingen oppsett";
}

bool Sted::finnesOppsett()
{
	if (sisteOppsett > 0)
		return true;
	else
		return false;
}

List * Sted::kopier(int nr)
{
	List* liste = nullptr;
	Sone* sone, *kopi;
	int ant;
	int type;

	if (nr <= sisteOppsett) {                        
		liste = new List(Sorted);                    // Lager ny sortert liste
		ant = oppsett[nr]->noOfElements();           // Setter ant lik antall elemeter i lista
		for (int i = 1; i <= ant; i++)
		{
			sone = (Sone*)oppsett[nr]->removeNo(i);  // Setter sone-peker lik objektet som ligger i lista
			type = sone->sjekkType();                // Setter riktig sone type

			if (type == 0)                          
			{

				kopi = new Stoler(*((Stoler*)sone), type);  // Lager en kopi av et nytt Stoler objekt

			}
			else
			{
				kopi = new Vrimle(*((Vrimle*)sone), type); // Lager en kopi av et nytt Vrimle objekt

			}
			oppsett[nr]->add(sone);                        // Legger tilbake sone objekter
			liste->add(kopi);                              // Fyller lista med kopierte objekter
		}
	}
	return liste;                                          // Returnerer lista
}

void Sted::nySone(int nr)		                           
{
	Stoler* stoler;
	Vrimle* vrimle;    
	char navn[STRLEN]; 
	char tekst[NVNLEN] = "0";
	char ch;           
	int i;   
	int type;


	cout << "Navn paa ny sone (0  - avslutt):  "; 

	cin.getline(navn, STRLEN);

	if (strcmp(tekst, navn) != 0) {                        // Sjekker at navnet ikke er 0
		do
		{
			cout << "Sonetype - S(toler) eller V(rimle): ";
			ch = lesKommando();

			switch (ch) {
			case 'S':
			{
				type = 0;                         // Setter type = 0 pga enum
				stoler = new Stoler(navn, type);  // Legger til et nytt Stoler objekt
				oppsett[nr]->add(stoler);         // Sender med data om oppsett til stoler
				break;
			}

			case 'V':
			{
				type = 1;                        // Setter type = 1 pga enum
				vrimle = new Vrimle(navn, type); // Legger til et nytt Vrimle objekt
				oppsett[nr]->add(vrimle);        // Sender med data om oppsett til vrimle
				break;
			}

			default: cout << "Feil N-kommando!\n";  break;
			}
			cout << "Navn paa ny sone (0  - avslutt):  ";
			cin.getline(navn, STRLEN);
			i = atoi(navn);                               // char til int


		} while (strcmp(tekst, navn) != 0);
	}
}

void Sted::fjernSone(int nr)
{
	char navn[NVNLEN];

	les("Sonenavn: ", navn, NVNLEN);

	if (oppsett[nr]->inList(navn))            // Sjekker om sonenavn finnes i lista
	{
		oppsett[nr]->remove(navn);            // Fjerner sonenavn fra lista
		cout << "Sone " << navn << " slettet";

	}
	else
		cout << "Finner ikke sonen";
}

void Sted::endreSone(int nr)
{
	Stoler* stoler;
	Vrimle* vrimle;
	char navn[NVNLEN];
	char ch;
	int type;

    do {
        les("Sonenavn: ", navn, NVNLEN);
    }
    while(!(oppsett[nr]->inList(navn)));                 
	
		oppsett[nr]->remove(navn);                       // Fjerner sonenavn fra lista

		cout << "Sonetype - S(toler) eller V(rimle) : ";

		ch = lesKommando();

		switch (ch) {
		case 'S':
		{	
			type = 0;                        // Setter type = 0 pga enum
			stoler = new Stoler(navn, type); // Legger til et nytt Stoler objekt
			oppsett[nr]->add(stoler);        // Sender med data om oppsett til stoler
			break;
		}

		case 'V':
		{
			type = 1;                        // Setter type = 1 pga enum
			vrimle = new Vrimle(navn, type); // Legger til et nytt Vrimle objekt
			oppsett[nr]->add(vrimle);        // Sender med data om oppsett til vrimle
			break;
		}

		default: cout << "\nFeil N-kommando!\n";  break;

		}
	}


void Sted::skrivTilFil(ofstream & ut)
{
	Sone* sone;


	ut << text << endl;
	ut << sisteOppsett << endl << endl;

	for (int i = 1; i <= sisteOppsett; i++)                 //looper gjennom antall oppsett
	{
		ut << i << endl;                                    //skriver ut antall opsett
		ut << oppsett[i]->noOfElements() << endl<<endl;         //antall soner i ett oppsett
		for (int j = 1; j <= oppsett[i]->noOfElements(); j++)   //looper gjennom antall soner i ett oppsett
		{
			sone = (Sone*)oppsett[i]->removeNo(j);              //fjerner fra liste
			if (sone->sjekkType() == 0)                          //hvis sonetype 0 stoler
			{
				oppsett[i]->add(sone);
			
				Stoler* stolerOB;
				stolerOB = (Stoler*)oppsett[i]->removeNo(j);    //peker po stolobjekt og fjerner fra liste
				stolerOB->skrivTilFil(ut);                      //skriver til fil
				oppsett[i]->add(stolerOB);                      //legger tilbake
			}
			else if(sone->sjekkType()==1)                       //hvis sonetype 1 vrimle
			{
				oppsett[i]->add(sone);
				Vrimle* vrimleOB;
				vrimleOB = (Vrimle*)oppsett[i]->removeNo(j);
				vrimleOB->skrivTilFil(ut);
				oppsett[i]->add(vrimleOB);
			}

		}
	}
}

bool Sted::finnesNavn(char * nvn)               //sammenligner text med medsendt navn
{
	return (!strcmp(text,nvn));   
}

void Sted::displayOppsett()                     //displayer oppsett
{
	int nr;

    if (sisteOppsett > 0) {                 //siste oppsett storre enn 0
        do {
            nr = les("Oppsetnummer: ", MINOPPSETT, sisteOppsett);   //leser inn onsket oppsett
			}
        while (nr > sisteOppsett);                                  //so lenge nr er for stort
			oppsett[nr]->displayList();                             //displayer liste
		}
		else
			cout << "Finnes ingen oppsett" << endl;
}

