#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <cctype> 
#include "globalFunksjonerHeadinger.h"
#include "Kunder.h"
#include "Steder.h"
#include "Arrangementer.h"
#include <fstream>
#include <cstring>

ifstream inn;
ofstream ut; 


extern Kunder kunder;
extern Steder steder;
extern Arrangementer arrangementer;
using namespace std;


													//HOVEDMENY
void skrivMeny() {								
	cout << "\t \t " << "KOMMANDOER: \n" << endl;
	cout << "\t " << "KUNDE:" << endl;
	cout << "K D\t" << "-Display" << endl;
	cout << "K N\t" << "-Ny" << endl;
	cout << "K E\t" << "-Endre" << endl << endl;

	cout << "\t " << "ARRANGEMENT:" << endl;
	cout << "A D\t" << "-Display" << endl;
	cout << "A N\t" << "-Nytt" << endl;
	cout << "A S\t" << "-Slett" << endl;
	cout << "A K\t" << "-Kjop" << endl << endl;

	cout << "\t " << "STED:" << endl;
	cout << "S D\t" << "-Display" << endl;
	cout << "S N\t" << "-Nytt" << endl << endl;

	cout << "\t " << "OPPSETT:" << endl;
	cout << "O D\t" << "-Display" << endl;
	cout << "O N\t" << "-Nytt" << endl;
	cout << "O E\t" << "-Endre" << endl << endl;

	cout << "Q - Quit\n\n";
	cout	<< "Hovedkommando: ";
}


char lesKommando() {					//Les enkel char funksjon
	char ch;							//char ch
	cin >> ch; cin.ignore();			//cin ch og ignore
	ch = toupper(ch);					//ch til stor char
	return ch;							//return stor bokstav
}


char les() {                           //  Henter ett ikke-blankt upcaset tegn:
	char ch;
	cin >> ch;   cin.ignore();         //  Leser ETT tegn. Forkaster '\n'.
	return (toupper(ch));              //  Upcaser og returnerer.
}


//  Leser et tall i et visst intervall:
int les(const char t[], const int min, const int max) {
	int n;
	do {                               // Skriver ledetekst:
		cout << '\t' << t << " (" << min << '-' << max << "): ";
		cin >> n;   cin.ignore();       // Leser inn ett tall.
	} while (n < min || n > max);      // Sjekker at i lovlig intervall.
	return n;                          // Returnerer innlest tall.
}


//  Leser en ikke-blank tekst:
void les(const char t[], char s[], const int LEN) {
	do {
		cout << '\t' << t << ": ";   cin.getline(s, LEN); // Ledetekst og leser.
	} while (strlen(s) == 0);          //  Sjekker at tekstlengden er ulik 0.
}

bool skuddaar(int aa) {            // Frode sin skuddaar funksjon
	                               //  Sjekker om et visst �r er skudd�r:
								   //  Skudd�r dersom: (delelig med 400) ELLER
								   //    (delelig med 4 OG ikke med 100)
	return ((aa % 400 == 0) || ((aa % 4 == 0) && (aa % 100) != 0));
}

int dagnummer(int da, int ma, int aa) { // Frode sin dagnummer funksjon
	//  Setter opp antall dager i m�nedene.
	//   Verdien for februar settes senere.
	int dagerPrMaaned[12] = { 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	int totalDagnr, i;

	if (aa < 2019 || aa > 2050) return 0;    //  Ulovlig �r.
	if (ma < 1 || ma > 12)   return 0;    //  Ulovlig m�ned.
									 //  Ut fra om �ret er skudd�r eller ei,
									 //    s� settes verdien for februar:
	dagerPrMaaned[1] = (skuddaar(aa)) ? 29 : 28;
	if (da < 1 || da > dagerPrMaaned[ma - 1])  return 0;  // Ulovlig dag:
									 //  Garantert at er en lovlig dato !!
	totalDagnr = da;
	for (i = 0; i < ma - 1; i++)             // Regner ut datoens dagnummer.
		totalDagnr += dagerPrMaaned[i];

	return totalDagnr;                       // Returnerer dagnummeret.
}

//leser data fra fil
 void lesFraFil(){	
	 inn = ifstream("KUNDER.DTA");
	 kunder.lesFraFil(inn);
	 inn.close();

	 inn = ifstream("STEDER.DTA");
	 steder.lesFraFil(inn);
	 inn.close();
     
     inn = ifstream("ARRANGEMENTER.DTA");
     arrangementer.lesFraFil(inn);
     inn.close();
     
}
 void genererNavn(char * a, const char  b[],const char c[], int nr)
	 //Limer sammer parametere + arr nummer
 {
	 char temp[STRLEN];
     
     sprintf(temp, "%d", nr);
     strcpy(a, b);

    
	 strcat(a, temp);
	 strcat(a, c);
 }

 void skrivTilFil(){
	 // skriver data til fil
	 ut = ofstream("KUNDER.DTA");
	 kunder.skrivTilFil(ut);
	 ut.close();
     
     ut = ofstream("ARRANGEMENTER.DTA");
     arrangementer.skrivTilFil(ut);
     ut.close();

	 ut = ofstream("STEDER.DTA");
	 steder.skrivTilFil(ut);
	 ut.close();
}
