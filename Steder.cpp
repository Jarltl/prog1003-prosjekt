#include "Steder.h"
#include "Sted.h"
#include "globalFunksjonerHeadinger.h"
#include <iostream>
#include <fstream>
#include "const.h"
#include "ListTool2B.h"
using namespace std;


Steder::Steder()                   //Steder klasse constructor
{
    StedListe = new List(Sorted);    //liste er sorted
}

void Steder::stedHandling()         //tar seg av sted handling. Display eller nytt sted
{
    char ch;
    ch = lesKommando();
    
    switch (ch)
    {
        case 'D': displayStedListe(); break;
        case 'N': nyttSted(); break;
        default:    cout << "Feil N-kommando!\n";  break;
    }
    
}

void Steder::oppsettHandling()
{
	char ch;
	Sted* sted;			                                //hjelpepeker
	char navn[NVNLEN];
	ch = lesKommando();
	
		
	les("Stedsnavn", navn, NVNLEN);	                    //leser navnet

		if (StedListe->inList(navn))	                //sjekker om stedet ligger i lista
		{
			sted = (Sted*)StedListe->remove(navn);		//tar ut fra lista

			sted->display();

			switch (ch)
			{
			case 'D':sted->displayOppsett(); break;			//displayer info om oppsett
			case 'N':sted->nyttOppsett(); break;	        //lager nytt oppsett
			case 'E':sted->endreOppsett(); break;	        //endrer paa oppsett
			case 'S':; break;						        //sletter oppsett
			default:    cout << "Feil N-kommando!\n";  break;
			}
			StedListe->add(sted);
		}
		else
		cout << "\nFinner ikke stedet! \nNy hovedkommando: ";
}


void Steder::displayStedListe()
{
	cout << "\nListe over steder: " << endl;
    
    StedListe->displayList();
    
    cout << "\nNy hovedkommando: ";
}

bool Steder::finnesSted(char * nvn)                 //retunerer true om sted finnes
{
	if (StedListe->inList(nvn))
        return true;
    else
        return false;
}

bool Steder::finnesOppsett(char * nvn)              //retunerer true om oppsett finnes
{
	Sted* sted;
	sted = (Sted*)StedListe->remove(nvn);
	if (sted->finnesOppsett())
	{
		StedListe->add(sted);
		return true;
	}
    else
        return false;

}

int Steder::hentAntOppsett(char * nvn)       //retunerer antall oppsett
{
	int nr = 0;
	Sted* sted;
    
	sted = (Sted*)StedListe->remove(nvn);    // Tar ut fra lista
	nr = sted->hentAntOppsett(nr);           // 
    StedListe->add(sted);                    // Legger tilbake i lista
	return nr;
}

void Steder::nyttSted()
{
    Sted* ny;
    char navn[NVNLEN];
    
    cout << "Navn paa nytt sted: ";
	cin.getline(navn, NVNLEN);
    
    if (StedListe->inList(navn)) {		             //sjekker om gitt sted finnes allerede
        cout << "\nSted finnes fra for av i lista!";
        cout << "\nNy kommando: ";}
	
	else
	{
		ny = new Sted(navn);			              //lager nytt sted
		StedListe->add(ny);				              //adder til lista
		cout << "Nytt sted er lagt til." << endl;
        cout << "\nNy hovedkommando: " ;
	}
}
void Steder::lesFraFil(ifstream& inn)
{
	int antallSteder = 0;
	char stedsnavn[NVNLEN];

	inn >> antallSteder; inn.ignore();

	if (antallSteder > 0)                        //er antallsteder storre enn 0
	{
	
		for (int i = 1; i <=antallSteder; i++)  //looper gjennom antall steder
		{
			inn >> stedsnavn;
			StedListe->add(new Sted(stedsnavn, inn));
		}
	}
	cout << " Lest inn: " << antallSteder << " fra STEDER.DTA" << endl;
}
void Steder::skrivTilFil(ofstream & ut)
{
	int antallSteder = 0;

	antallSteder = StedListe->noOfElements();

	if (antallSteder > 0)
	{
		ut << antallSteder << endl << endl;
		Sted* sted;
		for (int i = 1; i <= antallSteder; i++) {
			sted = (Sted*)StedListe->removeNo(i);
			sted->skrivTilFil(ut);
			StedListe->add(sted);
		}
	}
}
	
List* Steder::kopier(char* nvn, int nr)  {
    List* liste = NULL;                    // Liste-peker mot 0 
    Sted* sted;
    
    if ((sted = (Sted*) StedListe->remove(nvn)))  { // Hvis den kan ta ut fra lista
        liste = sted->kopier(nr);                   // Kopierer liste fra sted
        StedListe->add(sted);                       // Legger tilbake i lista
    }
    return liste;                                   // Returnerer lista
}


