#if !defined (__STEDER_H)
#define __STEDER_H

#include "ListTool2B.h"
#include "Sted.h"

using namespace std;

class Steder {
private:
	List* StedListe;             // Liste av steder

public:
	Steder();		             //Constructor
	void oppsettHandling();
    void stedHandling();
	void displayStedListe();
	bool finnesSted(char* nvn);
	bool finnesOppsett(char* nvn);
	int hentAntOppsett(char* nvn);
    void nyttSted();
    List* kopier(char* nvn, int nr);
    void lesFraFil(ifstream& inn);
    void skrivTilFil(ofstream& ut);
};


#endif
