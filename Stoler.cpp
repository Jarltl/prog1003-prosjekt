#include <iostream>
#include <fstream>
#include "Stoler.h"
#include "const.h"
using namespace std;

Stoler::Stoler(char* navn, int type):Sone(navn, type)	//constructor som sender navn 
														 //og type videre til sone sin constructor
{
	antallRader = les("Antall rader: ", MINRADER, MAXRADER);
	antallSeter = les("Antall seter: ", MINSETER, MAXSETER);

	totaltilsalgs = antallRader * antallSeter;			//setter total antall billetter til rader*seter
}

Stoler::Stoler(char * navn, int type, ifstream & inn)  : Sone(navn, type, inn)//constructor som leser fra fil
{
	
	inn >> antallRader;
	inn >> antallSeter;

	for (int i = 1; i <= antallRader; i++)
	{
		for (int j = 1; j <= antallSeter; j++)
		{
			inn >> bilett[i][j];
		}

	}
}

Stoler::Stoler(Stoler & s,int type) : Sone((Sone*) & s,type )					//constructor som tar Stoler-objekt som paramter
																				//og kopierer dets data
{
	antallRader = s.antallRader;
	antallSeter = s.antallSeter;

	totaltilsalgs = s.totaltilsalgs;

	for (int i = 1; i <= antallRader; i++)
		for (int j = 1; j <= antallSeter; j++)
			bilett[i][j] = 0;

}


void Stoler::regBillett(int knr)								//registrerer bilett
{
	int rad, sete, ant, tilg;
	tilg = totaltilsalgs - solgt;

	if (solgt < totaltilsalgs)									//sjekker om solgt er mindre enn totaltilsalgs
	{
		ofstream ut;
		ut.open("BILLETTER.DTA", ios::app);						//apner DTA fil og skriver pa slitten av fila

		ant = les("Antall billetter: ", 1, tilg);				//leser antall billetter

			ut << "Antall billetter: " << ant << endl << endl;
			for (int i = 1; i <= ant; i++)						//for loop gjennom bestilte billeter
			{
				rad = les("Onsket rad:", MINRADER, antallRader);				//spor om rad
				sete = les("Onsket sete:", MINSETER, antallSeter);				//spor om sete
				cout << endl;

				while (bilett[rad][sete] != 0)									//looper hvis plassen er tatt
				{
					cout << "Sete nr: " << sete << " pa Rad nr: " << rad << " er opptatt." << endl;

					rad = les("Onsket rad:", MINRADER, antallRader);
					sete = les("Onsket sete:", MINSETER, antallSeter);
					cout << endl;

				}
				solgt++;
				cout << endl << "Solgt!" << endl;
				bilett[rad][sete] = knr;										//setter kundenummer pa riktig rad og sete
				ut << "Rad: " << rad << " Sete: " << sete << endl << endl;		//skriver ut rad og sete nummer 
			}

	}	
	else
	{
		cout << endl << "ALLE BILLETTER UTSOLGT !!!" << endl << endl;
	}
}


void Stoler::display() {

	Sone::display();
	int tempBillett[MAXRADER][MAXSETER];

	char k = '-';								//'-' for ledig
	char l = 'X';								//'X'  for tatt
	cout << endl << endl;

	cout.width(11);								//flytter teksten til hoyre
	for (int k = 1; k <= antallSeter; k++)		//lopper gjennom seter
	{
		cout << k % 10;							//printer ut sete nummere
	}
	cout << endl;
	for (int i = 1; i <= antallRader; i++)		//looper gjonnom rader
	{
		cout.width(8);
		cout << right << i << ": ";				//starter a skrive fra hoyre side
		for (int j = 1; j <= antallSeter; j++)	//
		{
			if (bilett[i][j] != 0)				//hvis billett er solgt, sett til 'X'
				tempBillett[i][j] = l;
			else
				tempBillett[i][j] = k;			//eller setter til 'X' hvis ikke solgt
			cout << (char)tempBillett[i][j];
		}
		cout << endl;
	}
	cout << endl << endl;
}

bool Stoler::sjekkSolgt()
{
	if (solgt < totaltilsalgs)					 //hvis tilgjengelig billetter return true
		return true;
	else
		return false;
}


void Stoler::skrivTilFil(ofstream & ut)				//skriver ut til fil
{
	Sone::skrivTilFil(ut);
	ut << antallRader << endl;
	ut << antallSeter << endl;
	for (int i = 1; i <= antallRader; i++)
	{
		for (int j = 1; j <= antallSeter; j++)
		{
			ut << bilett[i][j] << " ";
		}

	}
	ut << endl << endl;
}

void Stoler::skrivTilFilOppsett (ofstream & ut)			//skriver ut info om billettsalget til fil 
{
	Sone::skrivTilFil(ut);
	ut << antallRader << endl;
	ut << antallSeter << endl;
	for (int i = 1; i <= antallRader; i++)
	{
		for (int j = 1; j <= antallSeter; j++)
		{
			ut << bilett[i][j] << " ";
		}

	}
	ut << endl << endl;
}

