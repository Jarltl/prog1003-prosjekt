#if !defined (__Arragementer_H)
#define __Arragementer_H

#include "ListTool2B.h"
#include "Arrangement.h"
#include <fstream>


using namespace std; 

class Arrangementer {

private:
	int sisteArrNr; // int som inneholder det siste arrangement nummeret

	List* ArrListe; // ListToll-peker som peker mot en liste med arrangementer sin data

public: 
	Arrangementer();
    void arrHandling();
	void displayArrHandling();
    void skrivMenyArr();
    void displayArrAlle();
    void displayArrNavn();
    void displaySpillested();
    void displayType();
    void displayMedArtist();
    void displayMedNr();
    void displayMedDato();
    void displayArrTekst(char* nvn); //
	void nyttArr();
	int genererNr();
    void slettArr();
    void kjopArr();
    void skrivTilFil(ofstream& ut);
    void lesFraFil(ifstream& inn);
};


#endif
