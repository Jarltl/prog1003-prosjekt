#if !defined (__VRIMLE_H)
#define __VRIMLE_H

#include "ListTool2B.h"
#include "Sone.h"


class Vrimle: public Sone {
private:
	int billett[MAXBILLETT];
public:
	Vrimle(char* navn, int type);
	Vrimle(char* navn, int type, ifstream& inn);
	Vrimle(Vrimle& v, int type);
    void display();
	bool sjekkSolgt();
    void regBillett(int knr);
    void skrivTilFil(ofstream & ut);
};



#endif


