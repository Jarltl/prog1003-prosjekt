#if !defined (__SONE_H)
#define __SONE_H

#include "ListTool2B.h"
#include "Steder.h"
#include "Sted.h"
#include "Enums.h"


class Sone : public TextElement {
protected:
    //sonenavn fra textElenemt
    int totaltilsalgs;
    int solgt;
    int pris;
	
	type soneType;


public:
	Sone();
	Sone(char* navn, int type);
	Sone(char * navn, int type, ifstream& inn);
	Sone(Sone* s, int type);
    void display();
	void skrivTilFil(ofstream& ut);
	bool sjekkType();
	bool hentNavn(char* nvn);
};



#endif

