#if !defined (__ARRANGEMENT_H)
#define __ARRANGEMENT_H

#include "ListTool2B.h"
#include "Arrangement.h"
#include <fstream>
#include "Enums.h"
using namespace std;
class Arrangement : public TextElement { // Arrangement arver fra TextElement

private: 
    //Sortert på navn fra ListTool2B
    int ArrNr;                  // unikt arrnummer
	char* spilleSted,* artist; // char-pekere
	int dato, Time, minutt;
	int oppsettNr;
    typeArrangement arrType;   
	List* oppsettKopi;         // Inneholder oppsett
public:
    Arrangement();
	Arrangement(char* nvn, int nr, int oppsettNr);
    Arrangement(char* navn, int type, ifstream& inn);
    void display();
    bool finnesNr(int d);
    bool finnesSpillested(char* nvn); 
    bool finnesArtist(char* nvn);    
    bool finnesDato(int d);           
    bool finnesArrNavn(char* nvn);    
    bool finnesArrtype(int type);
	int hentNr();
	void lesData(List* liste,char* stedsnavn);
	void slettOppsett();
    void registrereKjop(int knr, char* snavn);
    List* hentListe();
    void skrivTilFil(ofstream& ut); 
	void skrivTilFilOppsett(ofstream& ut);
	void skrivTilFilBillett(ofstream& ut);
};


#endif
