#if !defined(__CONST_H)
#define  __CONST_H

const int NVNLEN = 30;
const int STRLEN = 50;
const int MINOPPSETT = 1;
const int MAXOPPSETT = 5;
const int MINPRIS = 1;
const int MAXPRIS = 1000;
const int MINRADER = 1;
const int MAXRADER = 100;
const int MINSETER = 1;
const int MAXSETER = 100;
const int MAXKUNDER = 1000;
const int MINTLF = 11111111;
const int MAXTLF = 99999999;
const int MINPOST = 1111;
const int MAXPOST = 9999;
const int MINBILLETT = 10;
const int MAXBILLETT = 100000;
const int MINPLASSER = 1;
const int MAXPLASSER = 100000;
const int MINTIME = 0;
const int MAXTIME= 24;
const int MINMINUTT = 0;
const int MAXMINUTT = 60;
const int MINDAGER = 1;
const int MAXDAGER = 31;
const int MINMAANED = 1;
const int MAXMAANED = 12;
const int MINAAR = 2019;
const int MAXAAR = 2050;
#endif
