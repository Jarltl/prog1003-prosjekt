#if !defined (__STED_H)
#define __STED_H

#include "ListTool2B.h"
#include "const.h"
#include <fstream>

using namespace std;

class Sted : public TextElement {
private:
	//Navn fra textelement
	List* oppsett[MAXOPPSETT+1];    // List-peker som inneholder antall oppsett per sted
	int sisteOppsett = 0;           // Setter sisteOppsett lik 0

public:

	Sted();
    void display();
	Sted(char* navn);
    Sted(char* navn, ifstream& inn);
	void nyttOppsett();
	void nySone(int nr);
	bool finnesNavn(char* nvn);
	void displayOppsett();
	void endreOppsett();
	void endreOppsett(int nr);
	int hentAntOppsett(int ant);
	void kopierOppsett(int OppNr);
	bool finnesOppsett();
	void fjernSone(int nr);
	void endreSone(int nr);
	List* kopier(int nr);
	void skrivTilFil(ofstream& ut);
	
};



#endif
