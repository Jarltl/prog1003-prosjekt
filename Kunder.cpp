#include "Kunder.h"
#include "Kunde.h"
#include "globalFunksjonerHeadinger.h"
#include "const.h"
#include "ListTool2B.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <string>
using namespace std;

Kunder::Kunder()                        //kunder klasse constructor
{
    KundeListe = new List(Sorted);      //liste er sorted
    sisteKunder = 0;                    //null kunder ved construction
}

void Kunder::kundeHandling()            //tar seg av display, ny og endre kunde
{
    char ch;
    ch = lesKommando();
    
    switch (ch)
    {
        case 'D': displayKundeHandling();          break;
        case 'N': nyKunde();                       break;
        case 'E': endreKunde();                    break;
        default:    cout << "Feil N-kommando!\n";  break;
    }
    
}

void Kunder::displayKundeHandling()       //displayer en kunde med <nr>, alle kunder eller alle med <navn>
{
    char navn[NVNLEN];
    int n;
    Kunde* kundepeker;
    bool fantnr = false;
    bool fantnavn = false;
    
    cout << "Skriv: <nr> <navn> <blank> = alle: ";
    cin.getline(navn, NVNLEN);
    
    n = atoi(navn);                                           //gjør char til int
    
    if (n){                                                  //om int er input
        for (int index = 1; index <= KundeListe->noOfElements(); index++)
        {
            kundepeker = (Kunde*)KundeListe->removeNo(index);      //ta ut nr i i lista
            if (kundepeker->finnesNummer(n)){                      //retunerer true om nr finnes
                cout << "KUNDE MED NR: " << n << "\n";
                kundepeker->display();
                KundeListe->add(kundepeker);                       //legger tilbake i lista
                fantnr = true;                                     //bool blir satt til true. Fant kundenr
                cout << "\nNy hovedkommando: ";
            }
            else {
                KundeListe->add(kundepeker);                   //finner ikke kunde. Legger tilbake i liste
                }
        }
        if (!fantnr)                                            //hvis bool er false. Feilmelding
            cout << "\nKunne ikke finne: " << n << endl;
    }
    else if (!strlen(navn))                                     //hvis <blank>
        {
                displayKundeAlle();                         //displayer alle kunder og dems info
                cout << '\n';
                cout << "\nNy hovedkommando: ";
        }
    else                                                    //hvis ikke <blank> eller <nr>
    {
        for (int index = 1; index <= KundeListe->noOfElements(); index++) //loop antall element
        {
            kundepeker = (Kunde*)KundeListe->removeNo(index);     //ta ut nr i i lista
            if (kundepeker->finnesNavn(navn)){                    //hvis navn display alle med det navnet
                kundepeker->display();
                KundeListe->add(kundepeker);            //legger tilbake i liste
                fantnavn = true;                    //bool blir satt til true. Fant kundenavn
            }
            else {
                KundeListe->add(kundepeker);            //finner ikke kunde. Legger tilbake i liste
                }
        }
        if (!fantnavn)                                 //hvis bool er false. Feilmelding
            cout << "\nKunne ikke finne: " << navn << endl;
            cout << "\nNy hovedkommando: ";
    }
}

void Kunder::displayKundeAlle()
{
    cout << "\nALLE KUNDER: \n";
    KundeListe->displayList();
}

void Kunder::nyKunde()
{
    Kunde* ny;                                    //peker for ny kunde
    int tempKnr = 1;                              //alle nye kunder tildeles 1 som temptall
    
    while (KundeListe->inList(tempKnr) == 1)      //sjekker om en kunde allerede har temp-tallet
    {
        tempKnr++;                                //lager nytt tall med ett tall hoyere
    }
    sisteKunder++;

    ny = new Kunde(tempKnr);                      //constructer ny kunde med generert nummer
	KundeListe->add(ny);                          //putter ny kunde i lista
    
    cout << "\nNy kunde lagt til!";
    cout << "\nNy hovedkommando: ";
}


void Kunder::endreKunde()                       //endre eksisterende kunde
{
    int var;
    Kunde* endre;                               //kundepeker
    
    cout << "Hvilket nummer har kunden du onsker aa endre?\n";
    cout << "Svar: "; cin >> var;                                 //skriver inn nummer til kunde
    
    if (KundeListe->inList(var) == 1)                             //sjekker om kunde ligger i lista
    {
        endre = (Kunde*)KundeListe->remove(var);                  //fjerner kunde
        endre = new Kunde(var);                                   //endre skal bli ny kunde med nye data
        KundeListe->add(endre);                                   //legger kunde tilbake i lista
        cout << "\nKunde med nummer " << var << " endret";
        cout << "\nNy hovedkommando: ";
    }
    else
    {
        cout << "\nDenne kunden finnes ikke.";                     //kunden finnes ikke
        cout << "\nNy hovedkommando: ";
    }
}

char*  Kunder::hentNavn(int nr, char * nvn)                 //henter navn til kunde og retunerer
{
	Kunde* kunde;
	if (KundeListe->inList(nr)) {                           //om kunde er i liste
		kunde = (Kunde*)KundeListe->remove(nr);             //fjerner kunde
	
		nvn = kunde->hentNavn();

		KundeListe->add(kunde);                             //legge tilbake

    }
		return nvn;
}


bool Kunder::finnKundeKjop(int knr)             //retunerer true om kunde finnes
{
    Kunde* finn;
    
    if (KundeListe->inList(knr)) {
        finn = (Kunde*)KundeListe->remove(knr);
        finn->returnNr();
		KundeListe->add(finn);
        return true;
    }
    else
        return false;
}

void Kunder::lesFraFil(ifstream & inn) {
	int antallKunder = 0;	                        //antall kunder
	int kundenummer;
	inn >> antallKunder;                            //leser antall kunder i filen
	
	if (antallKunder > 0) {                         //er antall kunder storre en 0

		for (int i = 1; i <= antallKunder; i++) {            //loop gjennom alle kunder
			inn >> kundenummer; inn.ignore();                //leser kundenummer og ignorerer neste linje
			KundeListe->add(new Kunde(kundenummer, inn));    //adder ny kunde til liste
			
		}
	}

		cout << " Lest inn: " << antallKunder << " kunder fra KUNDER.DTA" << endl;
}


void Kunder::skrivTilFil(ofstream& ut) {
	int antallKunder = 0;

	antallKunder = KundeListe->noOfElements();          //henter antall objekter i lista
	if (antallKunder > 0) {                             //er antall kunder storre en 0
		ut << antallKunder << endl << endl;             //skriver ut antall kunder
		Kunde* kunde;						            //lager en ny peker til Kunde
		for (int i = 1; i <= antallKunder; i++) {		//looper gjennom alle kunder
			kunde = (Kunde*)KundeListe->removeNo(i);	//"trekker ut" kunden
			kunde->skrivTilFil(ut);						//skriver den ut til fil
			KundeListe->add(kunde);						//legger den tilbake 

		}
	}
}
